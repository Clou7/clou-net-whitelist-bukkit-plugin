package com.clou7.whitelist.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import com.clou7.whitelist.Main;

public class Whitelist implements Listener, CommandExecutor {
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("cnwl")){
			if(args.length == 2){
				if(args[0].toString().toLowerCase().equals("add")){
					if(!Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().isEmpty()){
						String player_uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().replaceAll("-", "");
						String whitelist_key = Main.plugin.getConfig().getString("whitelist_key");
						try{
							String url = "https://api.clou7.com/minecraft/whitelist/add";
							HttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
							HttpPost post = new HttpPost(url);

							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters.add(new BasicNameValuePair("key", whitelist_key));
							urlParameters.add(new BasicNameValuePair("uuid", player_uuid));

							post.setEntity(new UrlEncodedFormEntity(urlParameters));
							HttpResponse res = client.execute(post);
							
							if(res.getStatusLine().getStatusCode() == 200 || res.getStatusLine().getStatusCode() == 400){
								if(res.getStatusLine().getStatusCode() == 200){
									sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
								ChatColor.BOLD + ChatColor.RED + args[1] + ChatColor.RESET + " Has Been " +
											ChatColor.BOLD + ChatColor.GREEN + "Added " + ChatColor.RESET + "To The Whitelist!");
									return true;	
								}
								if(res.getStatusLine().getStatusCode() == 302){
									sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
											ChatColor.BOLD + ChatColor.RED + args[1] + ChatColor.RESET + " Is Already On The Whitelist!");
									return true;
								}
							} else {
								sender.sendMessage( ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + ChatColor.RED + "Error Adding User");
								return true;
							}
							
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + 
					ChatColor.RED + "You must use a " + ChatColor.BOLD + "valid " + ChatColor.RESET + ChatColor.RED + "username!");
						return true;
					}
				}
				if(args[0].toString().toLowerCase().equals("remove")){
					if(!Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().isEmpty()){
						String player_uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().replaceAll("-", "");
						String whitelist_key = Main.plugin.getConfig().getString("whitelist_key");
						try{
							String url = "https://api.clou7.com/minecraft/whitelist/remove";
							HttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
							HttpPost post = new HttpPost(url);

							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters.add(new BasicNameValuePair("key", whitelist_key));
							urlParameters.add(new BasicNameValuePair("uuid", player_uuid));

							post.setEntity(new UrlEncodedFormEntity(urlParameters));
							HttpResponse res = client.execute(post);
							
							if(res.getStatusLine().getStatusCode() == 200){
								if(res.getStatusLine().getStatusCode() == 200){
									sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
								    ChatColor.BOLD + ChatColor.RED + args[1] + ChatColor.RESET + " Has Been " +
									ChatColor.BOLD + ChatColor.RED + "Removed " + ChatColor.RESET + "From The Whitelist!");
									return true;	
								}
							} else {
								sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + ChatColor.RED + "Error Removing User");
								return true;
							}
							
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + 
					ChatColor.RED + "You must use a " + ChatColor.BOLD + "valid " + ChatColor.RESET + ChatColor.RED + "username!");
						return true;
					}
				}
				if(args[0].toString().toLowerCase().equals("check")){
					if(!Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().isEmpty()){
						String player_uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString().replaceAll("-", "");
						String whitelist_key = Main.plugin.getConfig().getString("whitelist_key");
						try{
							String url = "https://api.clou7.com/minecraft/whitelist/check";
							HttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
							HttpPost post = new HttpPost(url);

							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters.add(new BasicNameValuePair("key", whitelist_key));
							urlParameters.add(new BasicNameValuePair("uuid", player_uuid));

							post.setEntity(new UrlEncodedFormEntity(urlParameters));
							HttpResponse res = client.execute(post);
							
							if(res.getStatusLine().getStatusCode() == 200 || res.getStatusLine().getStatusCode() == 404){
								if(res.getStatusLine().getStatusCode() == 200){
									sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
										    ChatColor.BOLD + ChatColor.RED + args[1] + ChatColor.RESET + " Has " + ChatColor.BOLD + ChatColor.GREEN + "Been " +
											"Found " + ChatColor.RESET + "On The Whitelist!");
									return true;	
								}
								if(res.getStatusLine().getStatusCode() == 404){
									sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
										    ChatColor.BOLD + ChatColor.RED + args[1] + ChatColor.RESET + " Has" + ChatColor.BOLD + ChatColor.RED + " Not Been " +
											 "Found " + ChatColor.RESET + "On The Whitelist!");
									return true;
								}
							} else {
								sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + ChatColor.RED + "Error Checking Whitelist");
								return true;
							}
							
						} catch (Exception e) {
							System.out.println(e);
						}
					} else {
						sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " +
					ChatColor.RED + "You must use a " + ChatColor.BOLD + "valid " + ChatColor.RESET + ChatColor.RED + "username!");
						return true;
					}
				}
				sender.sendMessage(ChatColor.BOLD + "[" + ChatColor.GOLD +"CNWL" + ChatColor.RESET + ChatColor.BOLD + "] " + ChatColor.RED + "Invalid Argument: " + ChatColor.RESET + args[0]);
				return true;
			} else {
				sender.sendMessage("Proper Usage: " + ChatColor.BOLD + "/cnwl [" + ChatColor.GREEN + "Add" + ChatColor.WHITE + "/" + ChatColor.RED + "Remove" + ChatColor.WHITE + "/" + 
				ChatColor.BLUE + "Check" + ChatColor.WHITE + "] " + ChatColor.GOLD + "<UserName>");
				return true;
			}
		}
		return false;
	}
}
