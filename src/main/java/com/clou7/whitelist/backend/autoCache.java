package com.clou7.whitelist.backend;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.bukkit.configuration.file.FileConfiguration;
import org.json.JSONObject;

import com.clou7.whitelist.Main;

public class autoCache {
	
	FileConfiguration config = Main.plugin.getConfig();

	public void cache() {
		Timer timer = new Timer();
		
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				
				String clou_api = "https://api.clou7.com/minecraft/whitelist/list";
				String whitelist_key = config.getString("whitelist_key");
				
				try{
					
					HttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
					HttpPost post = new HttpPost(clou_api);
					
					List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
					urlParameters.add(new BasicNameValuePair("key", whitelist_key));
					
					post.setEntity(new UrlEncodedFormEntity(urlParameters));
					HttpResponse res = client.execute(post);
					
					if(res.getStatusLine().getStatusCode() == 200){
						
						String responseString = new BasicResponseHandler().handleResponse(res);
						JSONObject json_res = new JSONObject(responseString);
						
						File currentDirectory = new File(new File(".").getCanonicalPath());
						String cache_file = (currentDirectory+ File.separator + "plugins" + File.separator + "Clou-NET_Whitelist" + File.separator + "cache.json");
						File cache = new File(cache_file);
						
						if(cache.exists()){
							
							cache.delete();
							cache.createNewFile();
							FileWriter write_out = new FileWriter(cache_file);
							write_out.write(json_res.toString());
							write_out.flush();
							write_out.close();
						}
						
						if(!cache.exists()){
							
							cache.createNewFile();
							FileWriter write_out = new FileWriter(cache_file);
							write_out.write(json_res.toString());
							write_out.flush();
							write_out.close();
							
						}
						
						
					} else {
						System.out.println("Error Localy Caching Whitelist");
					}
					
				} catch (Exception e){
					e.printStackTrace();
				}

			}
		}, 10, 300000);
	}
}
