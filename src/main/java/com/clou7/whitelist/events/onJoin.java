package com.clou7.whitelist.events;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.clou7.whitelist.Main;

public class onJoin implements Listener{

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent join) throws Exception {
		
		FileConfiguration config = Main.plugin.getConfig();

		String player_uuid = join.getPlayer().getUniqueId().toString().replaceAll("-", "");
		String kick_message = config.getString("kick_message");
		String whitelist_key = config.getString("whitelist_key");
		
		String url = "https://api.clou7.com/minecraft/whitelist/check";
		HttpClient client = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("key", whitelist_key));
		urlParameters.add(new BasicNameValuePair("uuid", player_uuid));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse res = client.execute(post);

		if(res.getStatusLine().getStatusCode() != 200) {
			if(res.getStatusLine().getStatusCode() == 404){
				join.getPlayer().kickPlayer(kick_message);
				join.setJoinMessage(null);
			} else {
				
				File currentDirectory = new File(new File(".").getCanonicalPath());
				String cache_file = (currentDirectory+ File.separator + "plugins" + File.separator + "Clou-NET_Whitelist" + File.separator + "cache.json");
				File cache = new File(cache_file);
				
				if(cache.exists()){
					
					@SuppressWarnings("resource")
					BufferedReader brTest = new BufferedReader(new FileReader(cache_file));
					String text = brTest.readLine();
					
					if(!text.contains(player_uuid)){
						join.getPlayer().kickPlayer(kick_message);
						join.setJoinMessage(null);
					}
					
				} else {
					System.out.println("API is down and cache file is missing...What happend here?");
				}
				
			}
		}
	}
}
