package com.clou7.whitelist;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.clou7.whitelist.backend.autoCache;
import com.clou7.whitelist.commands.Whitelist;
import com.clou7.whitelist.events.onDisconnect;
import com.clou7.whitelist.events.onJoin;

public class Main extends JavaPlugin{
	
	FileConfiguration config = getConfig();
	public static Main plugin;
	
	@Override
	public void onEnable(){
		plugin = this;
		loadListeners();
		loadCommands();
		loadConfig();
		saveConfig();
		reloadConfig();
		new autoCache().cache();
		getLogger().info("Clou-NET Whitelisting Started");
	}
	
	@Override
	public void onDisable(){
		getLogger().info("Clou-NET Whitelisting Stopped");
	}
	
	public void loadCommands(){
		Bukkit.getPluginCommand("cnwl").setExecutor(new Whitelist());
	}
	
	public void loadListeners(){
		Bukkit.getServer().getPluginManager().registerEvents(new onJoin(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new onDisconnect(), this);
	}
	
	public void loadConfig(){
		config.addDefault("whitelist_key", "<key>");
		config.addDefault("kick_message", "<kick message>");
		config.options().copyDefaults(true);
	}
}
